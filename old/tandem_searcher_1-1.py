import os
from tral.sequence import sequence

datum = '28-02-2021'

cr_seq = sequence.Sequence.create(file = './cr_hawk-eagles.fasta', input_format = 'fasta')

#cr_seq = sequence.sequence_io.read_fasta('./cr_hawk-eagles.fasta', indices=None)

for seq in cr_seq:
	seq_repeats = seq.detect(denovo=True, detection={"detectors": ["TRF"]})
	seq.set_repeatlist(seq_repeats, "trf-denovo")

#tandem_repeats = [seq.detect(denovo = True, detection = {"detectors": ["TRF"]}) for seq in cr_seq]

#tandem_repeats[0].repeats[0].calculate_pvalues()

#print(cr_seq[0].get_repeatlist("denovo").repeats[0])

#tandem_repeats[0].write(output_format = "tsv", file = "./niels.tsv")

cr_seq[0].get_repeatlist("trf-denovo").repeats[0].calculate_pvalues()

print(cr_seq[0].get_repeatlist("trf-denovo").repeats[0])

#for element in tandem_repeats:
	#element.write(output_format = "tsv", file = "results_tandem.tsv")

'''
for x in cr_seq[0].get_repeatlist("trf-denovo"):
    for c in x.repeats:
        c.calculate_pvalues()
        print(c)
'''
