from Bio import SeqIO

gb_file = SeqIO.parse('./hawks-eagles/he_full_genome_28-01-2021.gb', 'gb')

t = ','

line_list = ["Accession" + t + "Name" + t + "CR" + t + "YCR" + t + "tRNA-Thr" + "CR" + "Pro" ]
for gb_record in gb_file:
	#Auslesen der Organismusart und Accessionnumber
	organism = gb_record.annotations["organism"]
	accession = gb_record.id

	gene_list_1 = []
	gene_list_2 = []

	cr = "False"
	cr_counter = 0

	ycr = "False"
	ycr_counter = 0
	dloop_counter = 0

	for feature in gb_record.features[1:]:
		#Füllen der 1 Liste mit den Feature-Typen
		gene_list_1.append(feature.type)
		#Füllen der 2 Liste mit den Qualifiern der Feature-Typen
		if "product" in feature.qualifiers.keys():
			gene_list_2.append(feature.qualifiers.get("product", "NONE"))
		elif "gene" in feature.qualifiers.keys():
			gene_list_2.append(feature.qualifiers.get("gene", "NONE"))
		elif "note" in feature.qualifiers.keys():
			gene_list_2.append(feature.qualifiers.get("note", "NONE"))
		elif "locus_tag" in feature.qualifiers.keys():
			gene_list_2.append(feature.qualifiers.get("locus_tag", "NONE"))
		else:
			gene_list_2.append("-")
		#Suche nach CR
		if feature.type.lower() == "d-loop":
			cr = "d-loop"
			dloop_counter += 1
		for key, value in feature.qualifiers.items():
			if "control" in str(value).lower():
				cr = str(value)
				cr_counter += 1
			elif "d-loop" in str(value).lower():
				print("LOOP")
			elif "cr1" in str(value).lower():
				cr = str(value)
				cr_counter += 1
			#Suche nach YCR
			if "pseudo" in str(value).lower():
				ycr = str(value)
				ycr_counter += 1
			elif "cr2" in str(value).lower():
				ycr = str(value)
				ycr_counter += 1
			elif "non" in str(value).lower():
				ycr = str(value) + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
				ycr_counter += 1


	#Ausgabe falls etwas mehr als einmal gezählt wurde!
	#if cr_counter > 1:
	#	print("VORSICHT: CR kommt " + str(cr_counter) + " mal vor! @" + organism + " " + accession)
	if ycr_counter > 1:
		print("VORSICHT: CR kommt " + str(ycr_counter) + " mal vor! @" + organism + " " + accession)
	if dloop_counter > 1:
		ycr = str(dloop_counter) + "x d-loop" + " !"
		print("VORSICHT: D-Loop kommt " + str(dloop_counter) + " mal vor! @" + organism + " " + accession)



	line_list.append(accession + t + organism + t + "++CR++" + t + "++YCR++" + t + str(gene_list_1))
	line_list.append(" " + t + " " + t + str(cr) + t + str(ycr) + t + str(gene_list_2))

hawk_analyse = open('OLD_hawk_ANALYSE.csv', 'w')
for line in line_list:
	hawk_analyse.write(line + '\n')
hawk_analyse.close()

