from Bio import SeqIO
from tral.sequence import sequence

class Sequence:

	def __init__(self, accession, start, end):
		self.sequence = None
		self.start = start
		self.end = end
		self.length = None
		self.annotations_within = None


class Bird:

	def __init__(self, genbank_entry):
		self.accession = None
		self.organism = None
		self.taxonomy = None
		self.og_full_genome = None
		self.full_genome = None
		self.control_region = None
		self.pseudo_control_region = None

	def add_trick(self, trick):
		self.tricks.append(trick)

	excel_list = ["Accession" + t + "Name" + t + "Taxonomy" + t + "CR length" + t + "YCR length" + t + "Genome Size" + t + "tRNA-Thr" + t + "CR" + t + "tRNA-Pro" + t + "tRNA-Glu" + t + "YCR" + t + "tRNA-Phe" + t + "Annotations Within Suspected CR Location" + t + "Annotations Within Suspected YCR Location"]


def double_finder(current_seq, list_seq):
	for seq in list_seq:
		if str(seq) == str(current_seq):
			return True
	return False

def trna_reader(string_trna, value, feature):
	if string_trna in str(value).lower():
		return [feature.location.start, feature.location.end, feature.location]
	else:
		return False

def feature_reader(feature):
	qualifiers = ["product", "gene", "note", "locus_tag"]
	name = None
	for entry in qualifiers:
		if entry in feature_c.qualifiers.keys():
			name = feature_c.qualifiers.get(entry, None)
	return name

gb_file = SeqIO.parse('./hawks-eagles/he_full_genome_11-12-2020.gb', 'gb')  # PC
# gb_file = SeqIO.parse('./hawks-eagles/he_full_genome_11-12-2020.gb', 'gb') #Laptop

t = ';'
datum = '05-03-2021'

# That's the place where we store the excel-magic
excel_list = ["Accession" + t + "Name" + t + "Taxonomy" + t + "CR length" + t + "YCR length" + t + "Genome Size" + t + "tRNA-Thr" + t + "CR" + t + "tRNA-Pro" + t + "tRNA-Glu" + t + "YCR" + t + "tRNA-Phe" + t + "Annotations Within Suspected CR Location" + t + "Annotations Within Suspected YCR Location"]

# That's the place where we store the already altered sequences (all starting with tRNA-PHE)
seq_list = []  # 0: accession Nr. + name, 1: full sequence, 2: cr sequence, 3: ycr sequence

# Important for our double_finder function
untouched_seq_list = []



for gb_record in gb_file:
##########################SHORT TIME STORAGE, UNTIL WE APPEND TO OUR LISTS ABOVE#################################
	# That's the place where we store organism name, taxonomy, accession nr. and sequence length
	organism = gb_record.annotations["organism"]
	taxonomy = gb_record.annotations["taxonomy"][14:]
	accession = gb_record.id
	record_len = len(gb_record.seq)

	#T-RNAs Locations
	thr = []  # 0: start, 1: end, 2: orientation (-/+)
	pro = []
	glu = []
	phe = []

	#CONTROL REGION
	cr_loc = [] #0: start, 1: end, 2: length
	annotations_within_cr_loc = [] #Here we store all annotations withing the cr location
	cr_seq = None #Here we store the cr sequence
	cr = False #Here we store if a cr is present (True) or not

	#PSEUDO CONTROL REGION
	ycr_loc = [] #0: start, 1: end, 2: length
	annotations_within_ycr_loc = [] #Here we store all annotations withing the ycr location
	ycr_seq = None #Here we store the ycr sequence
	ycr = False #Here we store if a ycr is present (True) or not
##################################################################################################################

	#Before we further progress with our gb_record entry we check if it's already prestented with it's sequence
	if double_finder(gb_record.seq, untouched_seq_list):
		continue
	untouched_seq_list.append(gb_record.seq)

	#T-RNA SEARCH
	for feature in gb_record.features[1:]:
		#We store them at line 28
		for key, value in feature.qualifiers.items():
			thr = trna_reader("trna-thr", value, feature) if trna_reader("trna-thr", value, feature) else thr
			pro = trna_reader("trna-pro", value, feature) if trna_reader("trna-pro", value, feature) else pro
			glu = trna_reader("trna-glu", value, feature) if trna_reader("trna-glu", value, feature) else glu
			phe = trna_reader("trna-phe", value, feature) if trna_reader("trna-phe", value, feature) else phe

	# Genome-Eichung: Every full genome sequence has to start with tRNA-Phe -> we ensure that here
	if phe[0] != 0:
		full_seq = str(gb_record.seq[phe[0]:record_len]) + str(gb_record.seq[0:phe[0]])
	else:
		full_seq = str(gb_record.seq)

	# CONTROL REGION
	if thr[1] != pro[0]:
		cr_loc = [thr[1] + 1, pro[0] - 1, (pro[0] - 1) - (thr[1] + 1)] #Here we get the location of the cr
		cr_seq = str(gb_record.seq[cr_loc[0]:cr_loc[1] + 1]) #Here we get the sequence of the cr
		cr = True
	else:
		cr_loc = "not found"
		cr = False

	# PSEUDO CONTROL REGION: It is a little bit more complicated, because sometimes (if the original genome isn't starting with phe) the ycr is at the ending and beginning of the genome
	if glu[1] != phe[0] and glu[1] != record_len:
		if phe[0] == 0:
			ycr_loc = [glu[1] + 1, record_len, record_len - (glu[1] + 1)] #Here we store the location of the ycr
			ycr_seq = gb_record.seq[ycr_loc[0]:ycr_loc[1] + 1] #Here we store the sequence of the ycr
			ycr = True
		elif phe[0] != 0:
			if phe[0] < glu[1]:
				ycr_loc = [glu[1] + 1, phe[0] - 1, (record_len - (glu[1] + 1)) + (phe[0] - 1)] #Here we store the location of the ycr
				ycr_seq = str(gb_record.seq[ycr_loc[0]:(record_len)]) + str(gb_record.seq[0:ycr_loc[1] + 1])
			else:
				ycr_loc = [glu[1] + 1, phe[0] - 1, (phe[0] - 1) - (glu[1] + 1)] #Here we store the location of the ycr
				ycr_seq = str(gb_record.seq[ycr_loc[0]:ycr_loc[1] + 1]) #Here we store the sequence of the ycr
			ycr = True
	else:
		ycr_loc = "not found"
		ycr = False

	#ANNOTATIONS WITHING SUSPECTED CR and YCR
	#We go through every base pair of our cr/ycr and look for annotations there
	for feature_c in gb_record.features[1:]:
		for search_cr_loc in range(cr_loc[0] - 1, cr_loc[1]):
			if feature_c.location.start == search_cr_loc:
				cr_name = feature_reader(feature_c)
				annotations_within_cr_loc.append([feature_c.location.start, feature_c.location.end, feature_c.type, cr_name])
		for search_ycr_loc in range(ycr_loc[0] - 1, ycr_loc[1]):
			if feature_c.location.start == search_ycr_loc:
				ycr_name = feature_reader(feature_c)
				annotations_within_ycr_loc.append([feature_c.location.start, feature_c.location.end, feature_c.type, ycr_name])

##################################TIME TO STORE STUFF IN LISTS#######################################################
	# Thats the final step where we append the full sequence, cr sequence and ycr sequence in our seq_list
	seq_list.append([">" + accession + " " + organism, full_seq, cr_seq, ycr_seq])

	# Thats the step where we append the everything to our excel_list -> for cvs
	excel_list.append(accession + t + organism + t + str(taxonomy) + t + str(cr_loc[2]) + t + str(ycr_loc[2]) + t +
		str(record_len) + t + str(thr[2]) + t + str(cr_loc) + t + str(pro[2]) + t + str(glu[2]) + t + str(
		ycr_loc) + t + str(phe[2]) + t + str(annotations_within_cr_loc) + t + str(annotations_within_ycr_loc))
#####################################################################################################################

################################TIME TO STORE OUR RESULTS IN FILES###################################################
#Here we start to store everything in a CSV-File vor our Excel-Magic
hawk_analyse = open('./' + datum + '/A_hawk_ANALYSE_tRNA.csv', 'w')
for line in excel_list:
	hawk_analyse.write(line + '\n')
hawk_analyse.close()

#Here we store all cr, ycr and full genome sequence in a fasta file
hawk_genome = open('./' + datum + '/full-genome_hawks-eagles.fasta', 'w')
hawk_cr = open('./' + datum + '/cr_hawk-eagles.fasta', 'w')
hawk_ycr = open('./' + datum + '/ycr_hawk-eagles.fasta', 'w')
for element in seq_list:
	hawk_genome.write(str(element[0]) + '\n')  #name
	hawk_genome.write(str(element[1]) + '\n')
	hawk_cr.write(str(element[0]) + '\n')  #name
	hawk_cr.write(str(element[2]) + '\n')
	hawk_ycr.write(str(element[0]) + '\n')  #name
	hawk_ycr.write(str(element[3]) + '\n')
hawk_genome.close()
hawk_cr.close()
hawk_ycr.close()
#####################################################################################################################
