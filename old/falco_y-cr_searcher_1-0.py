from Bio import SeqIO
from tral.sequence import sequence


gb_file = SeqIO.parse('./falcon/fa_full_genome_09-03-2021.gb', 'gb')  # PC
# gb_file = SeqIO.parse('./hawks-eagles/he_full_genome_11-12-2020.gb', 'gb') #Laptop

t = ';'
datum = '09-03-2021'


def double_finder(current_seq, list_seq):
	for seq in list_seq:
		if str(seq) == str(current_seq):
			return True
	return False


def feature_reader(feature):
	qualifiers = ["product", "gene", "note", "locus_tag"]
	name = None
	for entry in qualifiers:
		if entry in feature.qualifiers.keys():
			name = feature.qualifiers.get(entry, None)
	return name


class Sequence:

	def __init__(self):
		self.sequence = None
		self.start = None
		self.end = None
		self.length = None
		self.annotations_within = []

	def trna_searcher(self, og_full_genome, genbank_features, search_string):
		for feature in genbank_features[1:]:
			for key, value in feature.qualifiers.items():
				if search_string in str(value).lower():
					self.start = feature.location.start
					self.end = feature.location.end
					self.sequence = og_full_genome[self.start:self.end]
					self.length = len(self.sequence)

	def set_subsequence(self, og_full_genome, start, end):
		self.start = start
		self.end = end
		self.sequence = str(og_full_genome[self.start-1:self.end+1])
		self.length = len(self.sequence)

	def annotations_searcher(self, genbank_features, sequence_start, sequence_end):
		# ANNOTATIONS WITHING SUSPECTED CR and YCR
		# We go through every base pair of our cr/ycr and look for annotations there
		for feature_c in genbank_features[1:]:
			for current_location in range(sequence_start-10, sequence_end):
				if feature_c.location.start == current_location:
					name = feature_reader(feature_c)
					self.annotations_within.append([feature_c.type, name, feature_c.location.start, feature_c.location.end])

	def update_location(self, length):
		self.start = self.start - length
		self.end = self.end - length


class Bird:

	def __init__(self, genbank_entry, accession_nr):
		self.genbank_entry = genbank_entry
		self.accession = accession_nr
		self.organism = genbank_entry.annotations["organism"]
		self.taxonomy = genbank_entry.annotations["taxonomy"][14:]
		self.og_full_genome = genbank_entry.seq
		self.length = len(self.og_full_genome)
		self.trna_thr = Sequence()
		self.trna_pro = Sequence()
		self.trna_glu = Sequence()
		self.trna_phe = Sequence()
		self.full_genome = ""
		self.control_region = Sequence()
		self.pseudo_control_region = Sequence()

	def set_trna(self):
		self.trna_thr.trna_searcher(self.og_full_genome, self.genbank_entry.features, "trna-thr")
		self.trna_pro.trna_searcher(self.og_full_genome, self.genbank_entry.features, "trna-pro")
		self.trna_glu.trna_searcher(self.og_full_genome, self.genbank_entry.features, "trna-glu")
		self.trna_phe.trna_searcher(self.og_full_genome, self.genbank_entry.features, "trna-phe")

	def genome_calibration(self, calibration_point):
		if calibration_point != 0:
			self.full_genome = str(self.og_full_genome[calibration_point:self.length]) + str(self.og_full_genome[0:calibration_point])
			calibration_length = len(self.og_full_genome[0:calibration_point])
			return calibration_length
		else:
			self.full_genome = str(self.og_full_genome)
			return 0

	def set_cr(self, start, end):
		self.control_region.set_subsequence(self.og_full_genome, start, end)
		self.control_region.annotations_searcher(self.genbank_entry.features, start, end)

	def set_ycr_a(self, start, end, calibration_length):
		self.pseudo_control_region.set_subsequence(self.full_genome, start-calibration_length, end)
		self.pseudo_control_region.annotations_searcher(self.genbank_entry.features, start, end)

	def set_ycr_b(self, start, end, calibration_length):
		self.pseudo_control_region.set_subsequence(self.full_genome, start+(self.length-calibration_length), end)
		self.pseudo_control_region.annotations_searcher(self.genbank_entry.features, start, end)




# Important for our double_finder function
already_present_full_genomes = []

all_our_objects_list = []

for gb_record in gb_file:
	if double_finder(gb_record.seq, already_present_full_genomes):
		continue
	already_present_full_genomes.append(gb_record.seq)

	no_ycr_KICK_IT = False

	gb_record.id = Bird(gb_record, str(gb_record.id))

	gb_record.id.set_trna()

	#Here we calibrate all our genomes, which don't start with tRNA-Phe
	#We get the length of our calibration with calibration_length
	calibration_length = gb_record.id.genome_calibration(gb_record.id.trna_phe.start)

	# CONTROL REGION
	if gb_record.id.trna_thr.end != gb_record.id.trna_pro.start:
		gb_record.id.set_cr(gb_record.id.trna_thr.end+1, gb_record.id.trna_pro.start-1)
	else:
		no_ycr_KICK_IT = True
		print("cr not found")

	# PSEUDO CONTROL REGION: It is a little bit more complicated, because sometimes (if the original genome isn't starting with phe) the ycr is at the ending and beginning of the genome
	if gb_record.id.trna_glu.end != gb_record.id.trna_phe.start and gb_record.id.trna_glu.end != gb_record.id.length:
		if gb_record.id.trna_phe.start == 0:
			gb_record.id.set_ycr_a(gb_record.id.trna_glu.end+1, gb_record.id.length, calibration_length)
		else:
			if gb_record.id.trna_phe.start > gb_record.id.trna_glu.start:
				gb_record.id.set_ycr_b(gb_record.id.trna_glu.end+1, gb_record.id.length, calibration_length)
			else:
				gb_record.id.set_ycr_a(gb_record.id.trna_glu.end+1, gb_record.id.length, calibration_length)
		#We deduct the calibration legnth from the orginal ending of glu, because we deducted the basepairs before Phe (our calibration point)
		#and so every point in our sequence moved with the length of that
	else:
		no_ycr_KICK_IT = True
		print("ycr not found")

	if no_ycr_KICK_IT == False:
		all_our_objects_list.append(gb_record.id)

	#CALIBRATE TRNAS fehlt noch!!

	print(gb_record.id.trna_glu.end, gb_record.id.pseudo_control_region.start, gb_record.id.trna_phe.start, gb_record.id.pseudo_control_region.end, calibration_length)

################################TIME TO STORE OUR RESULTS IN FILES###################################################
#Here we start to store everything in a CSV-File vor our Excel-Magic
falco_analyse = open('./' + datum + '/A_falco_ANALYSE_tRNA.csv', 'w')

#Here we store all cr, ycr and full genome sequence in a fasta file
falco_genome = open('./' + datum + '/full-genome_falco.fasta', 'w')
falco_cr = open('./' + datum + '/cr_falco.fasta', 'w')
falco_ycr = open('./' + datum + '/ycr_falco.fasta', 'w')

falco_analyse.write("Accession" + t + "Name" + t + "Taxonomy" + t + "CR length" + t + "YCR length" + t + "Genome Size" + t + "tRNA-Thr" + t + "CR" + t + "tRNA-Pro" + t + "tRNA-Glu" + t + "YCR" + t + "tRNA-Phe" + t + "Annotations Within Suspected CR Location" + t + "Annotations Within Suspected YCR Location" + '\n')
for object in all_our_objects_list:
	falco_analyse.write(str(object.accession) + t + str(object.organism) + t + str(object.taxonomy) + t + str(object.control_region.length)+ t + str(object.pseudo_control_region.length) + t + str(object.length)+ t + str(object.trna_thr.end) + t + str(object.control_region.start) + t + str(object.trna_pro.start) + t + str(object.trna_glu.end) + t + str(object.pseudo_control_region.start) + t + str(object.trna_phe.start) + t + str(object.control_region.annotations_within) + t + str(object.pseudo_control_region.annotations_within) + '\n')

	falco_genome.write(">" + str(object.accession) + " " +str(object.organism) + " Full genome: \n")  #name
	falco_genome.write(str(object.full_genome) + '\n')

	falco_cr.write(">" + str(object.accession) + " " +str(object.organism) + " CR sequence: \n")  #name
	falco_cr.write(str(object.control_region.sequence) + '\n')

	falco_ycr.write(">" + str(object.accession) + " " + str(object.organism) + " YCR sequence: \n")  #name
	falco_ycr.write(str(object.pseudo_control_region.sequence) + '\n')

falco_genome.close()
falco_cr.close()
falco_ycr.close()

falco_analyse.close()
#####################################################################################################################
