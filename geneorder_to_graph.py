import matplotlib.pyplot as plt
import csv

t = "\t"

result = []

with open('./aves_ANALYSE.tsv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=t)
    for row in csv_reader:
        genus = row[1].split(" ", 1)
        order, family = row[2].split(" ", 2)
        if row[4] == "True" and genus not in result:
            result.append([order, family, genus, row[5], row[9], row[10], row[11], [12]])


################################TIME TO STORE OUR RESULTS IN FILES###################################################
# Here we start to store everything in a CSV-File for our Excel-Magic
analyse_file = open('./auswertung.tsv', 'w')

for element in result:
    for row in element:
        analyse_file.write(row, t)
    analyse_file.write("\t")
analyse_file.close()



fig, ax = plt.subplots()
ax.broken_barh([(110, 30), (150, 10)], (10, 9), facecolors='tab:blue')
ax.broken_barh([(10, 50), (100, 20), (130, 10)], (20, 9),
               facecolors=('tab:orange', 'tab:green', 'tab:red'))
ax.set_ylim(5, 35)
ax.set_xlim(0, 200)
ax.set_xlabel('seconds since start')
ax.set_yticks([15, 25])
ax.set_yticklabels(['Bill', 'Jim'])
ax.grid(True)
ax.annotate('race interrupted', (61, 25),
            xytext=(0.8, 0.9), textcoords='axes fraction',
            arrowprops=dict(facecolor='black', shrink=0.05),
            fontsize=16,
            horizontalalignment='right', verticalalignment='top')

plt.show()