# Script to rename tips in a phylo tree using tab delim name file
# Usage: python rename_nodes.py [names file] [tree file] [outfile]

import sys
import re

#Infile of names
names_in = open(sys.argv[1], 'r')

names_hash = {}

for line in names_in:
    line = line.rstrip('\n')
    old,new = line.split('\t')
    names_hash[old[:10]] = new

names_in.close()

# Inpfile of tree
tree_infile_path = sys.argv[2]
tree_in = open(tree_infile_path, 'r')

# outfile of tree
tree_outfile_path = sys.argv[3]

tree_out = open(tree_outfile_path, 'w')

for line in tree_in:
    for key in names_hash:
        print(key)
        if key in line:
            line = re.sub(key, names_hash[key], line)
            #print line
    tree_out.write(line)
print("New tree saved!")

tree_out.close()
