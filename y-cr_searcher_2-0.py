from Bio import SeqIO
from datetime import datetime
from tral.sequence import sequence
import pickle
import os
import re

print("+++ +++ +++ Y-CR SEARCHER +++ +++ +++")
genbank_input = input("Please input the Genbank input file name: ")
name_output = input("Please input the output file name prefix: ")

gb_file = SeqIO.parse('./genbank-input/' + genbank_input, 'gb')  # PC

t = '\t'
min_len_cr = 500
min_len_ycr = 100
min_len_genome = 15000

date = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
parent_directory = "./results/"
path = os.path.join(parent_directory, date)
os.mkdir(path)

error_file = open('./results/' + date + '/' + name_output + '_ERROR.txt', 'w')
names_file = open('./results/' + date + '/' + name_output + '_NAMES.txt', 'w')
analyse_file = open('./results/' + date + '/' + name_output + '_ANALYSE.tsv', 'w')
geneorder_file = open('./results/' + date + '/' + name_output + '_GENEORDER.csv', 'w')
cr_repeats_file = open('./results/' + date + '/' + name_output + '_CR_repeats.tsv', 'w')
ycr_repeats_file = open('./results/' + date + '/' + name_output + '_YCR_repeats.tsv', 'w')
cr_file = open('./results/' + date + '/' + name_output + '_CR.fasta', 'w')
ycr_file = open('./results/' + date + '/' + name_output + '_YCR.fasta', 'w')

analyse_file.write(
    "Accession" + t + "Name" + t + "Taxonomy" + t + "Genome Size" + t + "Genome complete?" + t + "Novel gene order?" + t + "Duplication remnants" + t + "CR length" + t + "YCR length" + t + "Genes" '\n')
geneorder_file.write(
    "Order;Genus;Genome complete?;Novel gene order?;Duplication remnants;Gene order" '\n')
cr_repeats_file.write(
    "No." + t + "Accession" + t + "Name" + t + "Taxonomy" + t + "Genome Size" + t + "Novel gene order?" + t + "CR length" + t + "Start" + t + "Region length" + t + "Number of repeats" + t + "Repeat length" + t + "Divergence" + t + "P-Value" + t + "Repeat" '\n')
ycr_repeats_file.write(
    "No." + t + "Accession" + t + "Name" + t + "Taxonomy" + t + "Genome Size" + t + "Novel gene order?" + t + "YCR length" + t + "Start" + t + "Region length" + t + "Number of repeats" + t + "Repeat length" + t + "Divergence" + t + "P-Value" + t + "Repeat" '\n')


##############################################################################################
def save_object(obj, filename):  # Damit speichern wir unsere Objektliste https://stackoverflow.com/questions/4529815/saving-an-object-data-persistence
    with open(filename, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)


##############################################################################################
def double_finder(current_seq, list_seq):
    for seq in list_seq:
        if str(seq) == str(current_seq):
            return True
    return False


##############################################################################################
def min_max_finder(list):
    max_element = max(list, default="None")
    min_element = min(list, default="None")
    if max_element == min_element:
        return str(max_element)
    else:
        return str(min_element) + " - " + str(max_element)


##############################################################################################
def feature_reader(feature_obj):
    qualifiers = ["product", "gene", "note", "locus_tag"]
    name = None
    for entry in qualifiers:
        if entry in feature_obj.qualifiers.keys():
            name = feature_obj.qualifiers.get(entry, None)
    return name


##############################################################################################
class Sequence:

    def __init__(self, name, og_full_genome, start, end):
        self.name = name
        self.start = start
        self.end = end
        self.sequence = og_full_genome[self.start:self.end]
        self.length = len(self.sequence)
        self.annotations_within = []
        self.repeats = []
        self.repeats_start = []
        self.repeats_number = []
        self.repeats_length = []
        self.repeats_region_length = []
        self.repeats_divergence = []
        self.repeats_pvalue = []

    def __eq__(self, other):
        if isinstance(other, Sequence):
            return self.name == other.name and self.start == other.start and self.end == other.end

    def set_subsequence(self, og_full_genome, start, end):
        self.start = start
        self.end = end
        self.sequence = str(og_full_genome[self.start - 1:self.end + 1])
        self.length = len(self.sequence)

    def annotations_searcher(self, genbank_features, sequence_start, sequence_end):
        # ANNOTATIONS WITHING SUSPECTED CR and YCR
        # We go through every base pair of our cr/ycr and look for annotations there
        # Deaktiviert, weil nur für Debug wichtig:
        """
        for feature_c in genbank_features[1:]:
            for current_location in range(sequence_start - 2, sequence_end):
                if feature_c.location.start == current_location:
                    name = feature_reader(feature_c)
                    self.annotations_within.append(
                        [feature_c.type, name, feature_c.location.start, feature_c.location.end])
        """

    def update_location(self, calibration_point_start, genome_len, cali_len):  # NOT DONE YET!!!
        if self.start >= calibration_point_start:
            self.start = self.start - cali_len
            self.end = self.end - cali_len
        else:
            self.start = self.start + (genome_len - cali_len)
            self.end = self.end + (genome_len - cali_len)

    def repeats_searcher(self, detector):
        filtered_seq = ''.join((filter(lambda x: x.lower() in ['a', 'c', 'g', 't'], self.sequence)))
        # We filter all characters which are not a, c, g or t because otherwise TRF crashes
        tandem_seq = sequence.Sequence(str(filtered_seq))
        tandems = tandem_seq.detect(denovo=True, detection={"detectors": [detector]})
        for r in tandems.repeats:
            r.calculate_pvalues()
            self.repeats.append(r.msa_original)
            self.repeats_start.append(r.begin)
            self.repeats_number.append(r.n)
            self.repeats_length.append(r.l_effective)
            self.repeats_region_length.append(r.repeat_region_length)
            self.repeats_divergence.append(r.divergence('phylo_gap01'))
            self.repeats_pvalue.append(r.pvalue('phylo_gap01'))

    def rename(self, name_str):
        self.name = name_str


##############################################################################################
class Bird:

    def __init__(self, genbank_entry, accession_nr):
        self.genbank_entry = genbank_entry
        self.ncbi_title = None
        self.author = None
        self.title = None
        self.journal = None
        self.accession = accession_nr
        self.organism = genbank_entry.annotations["organism"]
        self.taxonomy = genbank_entry.annotations["taxonomy"][14:]
        self.og_full_genome = genbank_entry.seq
        self.length = len(self.og_full_genome)
        self.full_genome = ""
        self.control_region = None
        self.pseudo_control_region = None
        self.genes = []

    def set_author_co(self):
        self.ncbi_title = str(gb_record.description).lower()
        for ref in gb_record.annotations["references"]:
            self.author = ref.authors
            self.title = ref.title
            self.journal = ref.journal

    def set_cr(self, start, end):
        if end-start >= min_len_cr:
            self.control_region = Sequence("CR", self.og_full_genome, start, end)
            self.genes.append(self.control_region)
            self.control_region.annotations_searcher(self.genbank_entry.features, start, end)

    def set_ycr_a(self, start, end, cali_length):
        self.pseudo_control_region = Sequence("YCR", self.og_full_genome, start - cali_length, end)
        self.genes.append(self.pseudo_control_region)
        self.pseudo_control_region.annotations_searcher(self.genbank_entry.features, start, end)

    def set_ycr_b(self, start, end, cali_length):
        self.pseudo_control_region = Sequence("YCR", self.og_full_genome, start + (self.length - cali_length), end)
        self.genes.append(self.pseudo_control_region)
        self.pseudo_control_region.annotations_searcher(self.genbank_entry.features, start, end)

    def genome_calibration(self, calibration_point):
        if calibration_point != 0:
            self.full_genome = str(self.og_full_genome[calibration_point:self.length]) + str(self.og_full_genome[0:calibration_point])
            calibration_len = len(self.og_full_genome[0:calibration_point])
            return calibration_len
        else:
            self.full_genome = str(self.og_full_genome)
            return 0

    def append_gene(self, name_str, og_full_genome_str, start, end):
        new_gene = Sequence(name_str, og_full_genome_str, start, end)
        if new_gene not in self.genes:
            self.genes.append(new_gene)

    def is_present(self, name_str):
        if self.genes:
            for gene_obj in self.genes:
                if gene_obj.name == name_str:
                    return True
            return False
        else:
            return False

    def is_doublet(self, name_str):
        gene_list = []
        if self.genes:
            for gene_obj in self.genes:
                if gene_obj.name == name_str:
                    gene_list.append(gene_obj)
            if len(gene_list) > 1:
                return True
            else:
                return False
        else:
            return False

    def gene(self, name_str, copy=False):
        gene_list = []
        for gene_obj in self.genes:
            if gene_obj.name == name_str:
                gene_list.append(gene_obj)
        if not gene_list:
            raise Exception("Gene was not found!")
        gene_list.sort(key=lambda seq_obj: seq_obj.start)
        if copy:
            if len(gene_list) <= 2:
                return gene_list[1]
            else:
                gen_doppelt_list.append(name_str)
                return gene_list[1]
                # raise Exception("Gene is present more than twice!")
        else:
            return gene_list[0]

    def sorted_genes(self):
        return sorted(self.genes, key=lambda seq_obj: seq_obj.start)

    def gene_searcher(self, features):
        # Here we search for our desired tRNAs + nd5, 6, rRNA12, cytB (sequence, position etc.) + annotated CR/YCR
        for feature in features:
            for key, value in feature.qualifiers.items():
                if "trna-thr" in str(value).lower() or "trnt" in str(value).lower():
                    self.append_gene("tRNA-Thr", self.og_full_genome, feature.location.start, feature.location.end)
                if "trna-pro" in str(value).lower() or "trnp" in str(value).lower():
                    self.append_gene("tRNA-Pro", self.og_full_genome, feature.location.start, feature.location.end)
                if "trna-glu" in str(value).lower() or "trne" in str(value).lower():
                    self.append_gene("tRNA-Glu", self.og_full_genome, feature.location.start, feature.location.end)
                if "trna-phe" in str(value).lower() or "trnf" in str(value).lower():
                    self.append_gene("tRNA-Phe", self.og_full_genome, feature.location.start, feature.location.end)
                if "nd5" in str(value).lower() or "nadh dehydrogenase subunit 5" in str(
                        value).lower() or "nadh 5" in str(value).lower():
                    self.append_gene("ND5", self.og_full_genome, feature.location.start, feature.location.end)
                if "nd6" in str(value).lower() or "nadh dehydrogenase subunit 6" in str(
                        value).lower() or "nadh 6" in str(value).lower():
                    self.append_gene("ND6", self.og_full_genome, feature.location.start, feature.location.end)
                if "12s" in str(value).lower():
                    self.append_gene("12S-rRNA", self.og_full_genome, feature.location.start, feature.location.end)
                if "cytochrome b" in str(value).lower() or "cytb" in str(value).lower() or "cyt b" in str(
                        value).lower():
                    self.append_gene("CytB", self.og_full_genome, feature.location.start, feature.location.end)
                if "pseudo" in str(value).lower() or "cr2" == str(value).lower() or "cr 2" == str(
                        value).lower() or "control region 2" in str(value).lower() or "control region II" in str(
                        value).lower():
                    self.append_gene("an.YCR", self.og_full_genome, feature.location.start, feature.location.end)
                elif "control" in str(value).lower() or "cr1" == str(value).lower() or "cr 1" == str(value).lower():
                    self.append_gene("an.CR", self.og_full_genome, feature.location.start, feature.location.end)


##############################################################################################

# Important for our double_finder function
genome_is_double = []
tRNA_is_missing = []
complete_objects = []
tRNAPhe_is_double = []

nc_entries = []
not_nc_entries = []

birds_geneorder_set = set()

for gb_record in gb_file:
    if "NC" == str(gb_record.id[:2]):
        nc_entries.append(gb_record)
    else:
        not_nc_entries.append(gb_record)

sorted_by_nc = nc_entries + not_nc_entries

for gb_record in sorted_by_nc:
    gen_doppelt_list = []

    if double_finder(gb_record.seq, genome_is_double):
        print(str(gb_record.id), ": Sequence occurred more than once!")
        error_file.write(str(gb_record.id) + ": Sequence occurred more than once! \n")
        continue
    genome_is_double.append(gb_record.seq)

    gb_record.id = Bird(gb_record, str(gb_record.id))

    if gb_record.id.length < min_len_genome:
        print(str(gb_record.id.accession), ": Incomplete Genome!")
        error_file.write(str(gb_record.id.accession) + ": Incomplete Genome! \n")
        tRNA_is_missing.append(gb_record.id)

    gb_record.id.set_author_co()  # We call to obtain all information about author, title and journal

    gb_record.id.gene_searcher(gb_record.features[1:])

    if gb_record.id.is_present("tRNA-Thr") is False or gb_record.id.is_present("tRNA-Pro") is False or gb_record.id.is_present("tRNA-Glu") is False or gb_record.id.is_present("tRNA-Phe") is False:
        print(str(gb_record.id.accession), ": Incomplete Genome!")
        error_file.write(str(gb_record.id.accession) + ": Incomplete Genome!")
        tRNA_is_missing.append(gb_record.id)
        # if gb_record.id.is_present("an.YCR"):
        #    gb_record.id.gene("an.YCR").repeats_searcher("TRF")
        # if gb_record.id.is_present("an.CR"):
        #    gb_record.id.gene("an.CR").repeats_searcher("TRF")
        continue

    if gb_record.id.is_doublet("tRNA-Phe"):
        print(str(gb_record.id.accession), ": tRNA-Phe occurred twice!")
        error_file.write(str(gb_record.id.accession) + ": tRNA-Phe occurred twice! \n")
        tRNAPhe_is_double.append(gb_record.id)
        continue

    # Here we calibrate all our genomes, which don't start with tRNA-Phe
    # We get the length of our calibration with calibration_length
    calibration_length = gb_record.id.genome_calibration(gb_record.id.gene("tRNA-Phe").start)

    # PSEUDO CONTROL REGION: It is a little bit more complicated, because sometimes (if the original genome isn't
    # starting with phe) the ycr is at the ending and beginning of the genome
    """
    if gb_record.id.is_doublet("tRNA-Glu"):
        if gb_record.id.gene("tRNA-Glu", True).end != gb_record.id.gene("tRNA-Phe").start:
            if gb_record.id.gene("tRNA-Phe").start == 0:
                gb_record.id.set_ycr_a(gb_record.id.gene("tRNA-Glu", True).end + 1, gb_record.id.length, calibration_length)
            else:
                if gb_record.id.gene("tRNA-Phe").start > gb_record.id.gene("tRNA-Glu", True).start:
                    gb_record.id.set_ycr_b(gb_record.id.gene("tRNA-Glu", True).end + 1, gb_record.id.length, calibration_length)
                else:
                    gb_record.id.set_ycr_a(gb_record.id.gene("tRNA-Glu", True).end + 1, gb_record.id.length, calibration_length)
                     # We deduct the calibration length from the original ending of glu, because we deducted the basepairs
                     # before Phe (our calibration point) and so every point in our sequence moved with the length of that
        else:
            print(str(gb_record.id.accession), ": YCR not found!")
            error_file.write(str(gb_record.id.accession) + ": YCR not found! \n")
    else:
        if gb_record.id.gene("tRNA-Glu").end != gb_record.id.gene("tRNA-Phe").start:
            if gb_record.id.gene("tRNA-Phe").start == 0:
                gb_record.id.set_ycr_a(gb_record.id.gene("tRNA-Glu").end + 1, gb_record.id.length, calibration_length)
            else:
                if gb_record.id.gene("tRNA-Phe").start > gb_record.id.gene("tRNA-Glu").start:
                    gb_record.id.set_ycr_b(gb_record.id.gene("tRNA-Glu").end + 1, gb_record.id.length, calibration_length)
                else:
                    gb_record.id.set_ycr_a(gb_record.id.gene("tRNA-Glu").end + 1, gb_record.id.length, calibration_length)
                    # We deduct the calibration length from the original ending of glu, because we deducted the basepairs
                    # before Phe (our calibration point) and so every point in our sequence moved with the length of that
        else:
            print(str(gb_record.id.accession), ": YCR not found!")
            error_file.write(str(gb_record.id.accession) + ": YCR not found! \n")
    """

    #Hier werden alle Gene kalibriert
    if calibration_length != 0:
        phe_start = gb_record.id.gene("tRNA-Phe").start
        for gene in gb_record.id.genes:
            if gene.name != "tRNA-Phe":
                gene.update_location(phe_start, gb_record.id.length, calibration_length)
        gb_record.id.gene("tRNA-Phe").update_location(phe_start, gb_record.id.length, calibration_length)
        # Muss immer zuletzt kalibriert werden, da es als Kalibrierungspunkt für den Rest gilt)

    # We're searching for all CR candidates, which are not found trough the typical Mindell novel gene order
    before_gene = None
    for gene in gb_record.id.sorted_genes():
        if gene.name == "an.CR" or gene.name == "an.YCR" or gene.name == "CR" or gene.name == "YCR" or gene.name == "12S-rRNA" or gene.name == "tRNA-Phe":
            continue
        elif before_gene is None:
            before_gene = gene
            continue
        else:
            if gene.start - before_gene.end > min_len_cr:
                gb_record.id.append_gene("CR", gb_record.id.og_full_genome, before_gene.end, gene.start-1)
                gb_record.id.gene("CR").repeats_searcher("TRF") # Searching for repeats:
            before_gene = gene
    if gb_record.id.length - before_gene.end > min_len_ycr:
        gb_record.id.append_gene("YCR", gb_record.id.og_full_genome, before_gene.end, gb_record.id.length)
        gb_record.id.gene("YCR").repeats_searcher("TRF") # Searching for repeats:


# WRITE TO FILE #
    duplication_remnants = False
    genome_complete = None
    novel_order = None
    cr_len = 0
    ycr_len = 0

    #Check for duplication remnants:
    if gb_record.id.is_doublet("tRNA-Glu") or gb_record.id.is_doublet("tRNA-Pro") or gb_record.id.is_doublet("tRNA-Thr") or gb_record.id.is_doublet("ND6") or gb_record.id.is_doublet("ND5") or gb_record.id.is_doublet("CytB") or gb_record.id.is_doublet("12S"):
        duplication_remnants = True

    if "complete genome" in gb_record.id.ncbi_title or "full genome" in gb_record.id.ncbi_title:
        genome_complete = True
    elif "partial genome" in gb_record.id.ncbi_title:
        genome_complete = False

    #Fasta to File
    if gb_record.id.is_present("CR") and gb_record.id.gene("CR").length > min_len_cr:
        novel_order = False
        cr_len = gb_record.id.gene("CR").length
        cr_file.write(">" + str(gb_record.id.accession) + ";" + str(gb_record.id.organism) + t + str(
            gb_record.id.taxonomy[0]) + t + "Novel gene order CR: \n")  # name
        cr_file.write(str(gb_record.id.gene("CR").sequence) + '\n')
        if gb_record.id.is_present("YCR"):
            novel_order = True
            ycr_len = gb_record.id.gene("YCR").length
            ycr_file.write(">" + str(gb_record.id.accession) + ";" + str(gb_record.id.organism) + t + str(
                gb_record.id.taxonomy[0]) + t + "Novel gene order YCR: \n")  # name
            ycr_file.write(str(gb_record.id.gene("YCR").sequence) + '\n')
    elif gb_record.id.is_present("YCR") and gb_record.id.gene("YCR").length:
        # Hier werden alle known gene order CR (welche als YCR sortiert wurden) umsortiert
        # Das hier ist unser Bereich für Known gene order CRs die als YCR falsch annotiert wurden von mir
        novel_order = False
        cr_len = gb_record.id.gene("YCR").length
        cr_file.write(">" + str(gb_record.id.accession) + ";" + str(gb_record.id.organism) + t + str(
            gb_record.id.taxonomy[0]) + t + "Known gene order CR: \n")  # name
        cr_file.write(str(gb_record.id.gene("YCR").sequence) + '\n')
        if gb_record.id.is_present("CR"):        
            gb_record.id.gene("CR").rename("!CR")
        gb_record.id.gene("YCR").rename("CR")
    else:
        if gb_record.id.is_present("YCR"):
            ycr_len = gb_record.id.gene("YCR").length
        if gb_record.id.is_present("CR"):
            cr_len = gb_record.id.gene("CR").length

    #Repeats to file
    if novel_order:
        crr = gb_record.id.gene("CR")
        ycrr = gb_record.id.gene("YCR")
        for i in range(len(gb_record.id.gene("CR").repeats_start)):
            cr_repeats_file.write(
                str(i) + t + str(gb_record.id.accession) + t + str(gb_record.id.organism) + t + str(
                    gb_record.id.taxonomy) + t + str(gb_record.id.length) + t + str(novel_order) + t + str(cr_len) + t + str(
                    crr.repeats_start[i]) + t + str(crr.repeats_region_length[i]) + t + str(
                    crr.repeats_number[i]) + t + str(crr.repeats_length[i]) + t + str(
                    crr.repeats_divergence[i]) + t + str(crr.repeats_pvalue[i]) + t + str(crr.repeats[i]) + t + "\n")
        for i in range(len(gb_record.id.gene("YCR").repeats_start)):
            ycr_repeats_file.write(
                str(i) + t + str(gb_record.id.accession) + t + str(gb_record.id.organism) + t + str(
                    gb_record.id.taxonomy) + t + str(gb_record.id.length) + t + str(novel_order) + t + str(ycr_len) + t + str(
                    ycrr.repeats_start[i]) + t + str(ycrr.repeats_region_length[i]) + t + str(
                    ycrr.repeats_number[i]) + t + str(ycrr.repeats_length[i]) + t + str(
                    ycrr.repeats_divergence[i]) + t + str(ycrr.repeats_pvalue[i]) + t + str(ycrr.repeats[i]) + t + "\n")

    analyse_file.write(
        gb_record.id.accession + t + str(gb_record.id.organism) + t + str(gb_record.id.taxonomy) + t + str(
            gb_record.id.length) + t + str(genome_complete) + t + str(novel_order) + t + str(
            duplication_remnants) + t + str(cr_len) + t + str(ycr_len) + t)
    current_gene_order_str = str(gb_record.id.taxonomy[0]) +";"+ str(gb_record.id.taxonomy[-1]) +";"+ str(genome_complete) +";"+ str(novel_order) +";"+ str(duplication_remnants)
    current_gene_order_str_end = ""
    for g in gb_record.id.sorted_genes():
        if g.name != "!CR" and g.name != "!YCR":
            analyse_file.write(str(g.name) + "(" + str(g.start) + "-" + str(g.end) + ")" + t)
            if g.name == "tRNA-Phe" or g.name == "12S-rRNA":
                current_gene_order_str_end += ";" + str(g.name)
            elif g.name != "an.CR" and g.name != "an.YCR":
                current_gene_order_str += ";" + str(g.name)
    analyse_file.write("\n")

    birds_geneorder_set.add(current_gene_order_str + current_gene_order_str_end)

    error_file.write(str(gb_record.id.accession) + " following genes occur more than twice: " )
    for element in gen_doppelt_list:
        error_file.write(str(element) + " ")

    names_file.write(
        str(gb_record.id.accession) + " " + str(gb_record.id.organism) + "\t" + str(gb_record.id.organism) + " " + str(
            gb_record.id.accession) + "\n")

    complete_objects.append(gb_record.id)
    # Je nachdem ob es am Anfang oder am Ende steht sind auch die mit fehlenden tRNAs dabei!

save_object(complete_objects, './results/' + date + '/' + name_output + '_objects.pkl')

analyse_file.close()
names_file.close()
cr_repeats_file.close()
ycr_repeats_file.close()
cr_file.close()
ycr_file.close()

for y in birds_geneorder_set:      
    geneorder_file.write(y + "\n")
geneorder_file.close()

for x in tRNAPhe_is_double:
    error_file.write("tRNAPhe was doubled in " + str(x.accession))
error_file.close()
print("+++ +++ +++ Done! Thanks for using Y-CR searcher! +++ +++ +++")
