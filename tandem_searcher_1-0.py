import os
from tral.sequence import sequence
from Bio import SeqIO

print("+++ +++ +++ FASTA MANIPULATOR +++ +++ +++")

t = "\t"

request = "Yes"
while "y" in request.lower():
    fasta_input = input("Please input the FASTA input file name: ")
    fasta_file = SeqIO.parse('./' + fasta_input, 'fasta')  # PC
    what_to_do = input("For manipulating your FASTA sequences input '1'. For searching for tandem repeats input '2'. For extracting a genus input '3': ")
    if what_to_do == "1":
        name_output = input("Please input the output file name prefix: ")
        min_len = input("Input the minimum length for each sequence: ")
        start = input("Input the start point for each sequence: ")
        end = input("Input the end point for each sequence: ")
        cutter_file = open('./' + name_output + '_CUT.fasta', 'w')
        for fasta in fasta_file:
            if len(fasta.seq) >= int(min_len):
                cutter_file.write(">" + str(fasta.description) + "\n")
                cutter_file.write(str(fasta.seq[int(start):int(end)]) + "\n")
            else:
                print(fasta.id + " was too short!")
                continue
        cutter_file.close()
    elif what_to_do == "2":
        name_output = input("Please input the output file name prefix: ")
        detector = input("Please input the de novo tandem repeat detection software you want to use (Phobos, T-REKS, TRF or XSTREAM): ")
        repeat_file = open('./' + name_output + '_' + detector + '_repeats.tsv', 'w')
        repeat_file.write("Accession" + t + "Name" + t + "Info" + t + "Sequence length" + t + "Repeat start" + t + "Number of repeat units" + t + "Effective length of repeat units" + t + "Repeat region length" + t + "Divergence" + t + "p-Value" + t + "Repeat" + "\n")
        for fasta in fasta_file:
            fasta_id, fasta_name, fasta_additional = fasta.description.split(t)
            filtered_seq = ''.join((filter(lambda x: x.lower() in ['a', 'c', 'g', 't'], fasta.seq))) #We filter all characters which are not a, c, g or t because otherwise TRF crashes
            print(fasta_id)
            tandem_seq = sequence.Sequence(str(filtered_seq))
            tandems = tandem_seq.detect(denovo=True, detection={"detectors": [detector]})
            repeats_found = []
            for r in tandems.repeats:
                r.calculate_pvalues()
                repeat_file.write(str(fasta_id) + t + str(fasta_name) + t + str(fasta_additional) + t + str(len(fasta.seq)) + t + str(r.begin) + t + str(r.n) + t + str(r.l_effective) + t + str(r.repeat_region_length) + t + str(r.divergence('phylo_gap01')) + t + str(r.pvalue('phylo_gap01')) + t + str(r.msa_original) + "\n")
                repeats_found.append(fasta_id)
            if fasta_id not in repeats_found:
                repeat_file.write(str(fasta_id) + t + str(fasta_name) + t + str(fasta_additional) + t + str(len(fasta.seq)) + t + "not found" + "\n")
        repeat_file.close()
    elif what_to_do == "3":
        genus = input("Input the desired genus you're searching for: ")
        genus_file = open('./' + '_' + genus + '_filter.fasta', 'w')
        for fasta in fasta_file:
            fasta_id, fasta_family, fasta_additional = fasta.description.split(t)
            fasta_genera = fasta_id.split(";", 1)
            if str(genus).lower() in str(fasta_genera[1]).lower() or str(genus).lower() in str(fasta_family).lower():
                genus_file.write(">" + str(fasta.description) + "\n")
                genus_file.write(str(fasta.seq) + "\n")
            else:
                print(str(genus) + " not found in " + str(fasta_id))
        genus_file.close()
    request = input("Do you want to restart the manipulator? ")

print("+++ +++ +++ done +++ +++ +++")

'''
https://acg-team.github.io/tral/background.html?highlight=divergence
https://acg-team.github.io/tral/significance_test.html
'''
