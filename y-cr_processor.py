import csv

t = "\t"

result = []

with open('./aves_ANALYSE.tsv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=t)
    for row in csv_reader:
        genus = row[1].split(" ", 1)
        order, family = row[2].split(" ", 2)
        if row[4] == "True" and genus not in result:
            result.append([order, family, genus, row[5], row[9], row[10], row[11], [12]])


################################TIME TO STORE OUR RESULTS IN FILES###################################################
# Here we start to store everything in a CSV-File for our Excel-Magic
analyse_file = open('./auswertung.tsv', 'w')

for element in result:
    for row in element:
        analyse_file.write(row, t)
    analyse_file.write("\t")
analyse_file.close()

#####################################################################################################################
# print("Following entries were sorted out, due to being double:", genome_is_double[1])
print("+++ +++ +++ Done! +++ +++ +++")
