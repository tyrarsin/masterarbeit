import csv

t = ";"

result = set()

with open('./aves_GENEORDER.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=t)
    for row in csv_reader:
        string = ""
        for element in row[5:]:
            string += element + " "
        result.add(string)

for e in result:
    print(e)